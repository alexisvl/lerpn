# Hacking on lerpn

## Architecture notes

lerpn is of course stack-based. In `nums.py`, you'll find a class `UndoStack`.
This is a subclass of `list` that implements "list of lists" behavior: you can
save a clone of the list with `.undopush()`, and restore the clone with `.undo()`.

Normally, you don't have to interact with this unless you want to.

## Adding commands

Commands are in `commands.py`. These are run in an exception-catching wrapper that
restores the pre-command state of the stack on exception, so don't worry about
catching exceptions in them (in fact, throw your own to show errors!).

There are two ways to implement a command.

### Defining commands from scratch

Each command is a function of the format `def _name(stack, arg=None)`. It is
passed the UndoStack instance in `stack`, and if run as a long-format command,
it is passed its string argument in `arg`. You do whatever modifications you want
to the stack, and don't return anything.

The command function must contain a documentation string. The first line of this
string will be used as the help text when the user calls up help via `?` or
`'help`.

Here is an example for addition:

```python
def _add (stack, arg=None):
    """addition of two numbers"""
    addend_right = stack.pop()
    addend_left = stack.pop()
    stack.append(addend_left + addend_right)
```

See the note about Tagged Numbers - you might have to handle them manually if
you do this!

### Defining simple unary and binary operators

If all you want to do is take convert `x` to `y`, or `lhs` and `rhs` to `z`,
you can use the `BINARY(f, docstring)` and `UNARY(f, docstring)` helpers. These
take care of everything, so you can just write a lambda function that does your
job and pass it in. Here's how you could define a unary function that cubes a
number:

```python
_cube = UNARY(lambda x: x ** 3, "cube")
```

### Warning about types

All numeric types in the stack must be either `Scalar` or `Comflex` (both are
custom types wrapping other standard types). Do not put integers or floats on
the stack. Several functions directly inspect the type of stack contents and
may misbehave.

There are two main reasons for avoiding all other types. First, there is no
visual indicator of whether something is e.g. a DecimalScalar, float, or int, so
mixing them on the stack would be very unclear. Second, bigints in Python have
no memory consumption limit, which could lead to lerpn very easily eating all
the system resources for a simple operation. These have limited precision.

### Adding your command to the list

Now that you've defined a command, you have to add it to the list so it can be
called. `commands.py` contains two OrderedDicts: `SINGLE_KEY_COMMANDS`, and
`COMMANDS`. The former contains commands that are called with a single keypress,
and the latter contains named commands. Add an entry for your command like this:

```python
    # For single-key commands
    ("L", command_function),

    # For named commands
    ("'logn", command_function),
```

Note how the named command begins with a single-quote. This is required!

## Tagged Numbers

Items in the stack can be instances of `Tagged`, which is found in `nums.py`.
This is the case when the user adds a string 'tag' to a value. The `Tagged`
class is a simple class containing members `.num`, which holds the value,
and `.tag`, which holds the string.

In certain cases, the desired behavior when doing math on tagged numbers is
obvious or already defined. `Tagged` overloads the following binary operators
at the time this document was written:

* `__add__`, `__radd__`
* `__sub__`, `__rsub__`
* `__mul__`, `__rmul__`
* `__truediv__`, `__rtruediv__`
* `__mod__`, `__rmod__`
* `__pow__`, `__rpow__`

The behavior of these is: A binary operation on (Tagged, untagged) returns a
new Tagged bearing the same tag. A binary operation on (Tagged, Tagged) returns
an untagged.

The `UNARY` helper preserves tags. The `BINARY` helper does not treat `Tagged`
specially, since the class overloads the operators.

If these cases do not fit your application, you will have to handle `Tagged`
manually.

**WARNING**: Note that `Tagged` implements `.num` and `.tag` as properties with
only getters, not setters. This is for a very good reason: if you change a
`Tagged`, you are going back through undo history and changing it there too!
**Never** attempt to override this behavior. Always create a new object to hold
a new value.
