from setuptools import setup

import sys

setup(  name='lerpn',
        version='5',
        description='Linux? Engineering RPN calculator',
        url='https://gitlab.com/alexisvl/lerpn',
        author='Alexis Lockwood',
        author_email='no@no.no',
        packages=['LerpnApp'],
        entry_points = {
            'console_scripts': [
                'lerpn = LerpnApp:main',
                ],
        },
        keywords = ['calculator', 'rpn'],
        classifiers = [
            "Development Status :: 5 - Production/Stable",
            "Environment :: Console :: Curses",
            "Topic :: Utilities",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)" ]
        )
