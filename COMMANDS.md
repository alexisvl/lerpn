# Quick Command Reference

This list is auto-generated from the code, and contains the same
text you would see in the in-program help box.

## Single-key commands

Key | Description
--- | -----------
[bksp] | drop
[enter] | dup
[up] | rotate up
[down] | rotate down
`-` | subtract
`+` | add
`*` | multiply
`/` | divide
`%` | modulo
`^` | power
`E` | exponential
`l` | log base e
`L` | log base 10
`r` | sq root
`i` | reciprocal
`n` | negate
`!` | factorial
`P` | permutations
`C` | combinations
`<` | shift left
`>` | shift right
`&` | bitwise AND
`|` | bitwise OR
`X` | bitwise XOR
`~` | invert bits
`u` | undo
`R` | redo
`x` | exchange/swap
`[` | dup item by number
`{` | move item from index to front
`}` | move item from front to index
`t` | attach tag to number
`T` | unTag
`?` | display help

## Long commands

Name | Description
---- | -----------
 | **==TRIGONOMETRY, RAD==**
`'sin` | sin(rad)
`'cos` | cos(rad)
`'tan` | tan(rad)
`'asin` | asin->rad
`'acos` | acos->rad
`'atan` | atan->rad
 | **==TRIGONOMETRY, DEG==**
`'sind` | sin(deg)
`'cosd` | cos(deg)
`'tand` | tan(deg)
`'asind` | asin->deg
`'acosd` | acos->deg
`'atand` | atan->deg
`'deg` | rad->deg
`'rad` | deg->rad
 | **==HYPERBOLICS==**
`'sinh` | sinh
`'cosh` | cosh
`'tanh` | tanh
`'asinh` | asinh
`'acosh` | acosh
`'atanh` | atanh
 | **==SIMPLE==**
`'exp` | exponential
`'log` | log base e
`'log10` | log base 10
`'log2` | log base 2
`'logn` | log base Y
`'divmod` | (y/x, y%x)
`'f` | resolve to the nearest fraction
`'n` | resolve to the nearest non-fraction
`'err` | error X=act Y=nom
`'perr` | pct error X=act Y=nom
`'unsigned` | Y to unsigned in X-bit
`'signed` | Y to signed in X-bit
`'floor` | floor
`'ceil` | floor
 | **==COMPLEX==**
`'re` | real part
`'im` | imaginary part
`'conj` | complex conjugate
`'reim` | split into real and imaginary parts
`'abs` | absolute value
`'angle` | angle/phase (rad)
`'polar` | split into absolute value and angle
`'fcart` | make complex from Cartesian real, imag
`'fpolar` | make complex from polar abs val, angle(rad)
 | **==CONSTANTS==**
`'pi` | const PI
`'2pi` | const 2 PI
`'e` | const E
`'j` | imaginary unit
`'c` | speed of light, m/s
`'h` | Planck constant, J s
`'k` | Boltzmann constant, J/K
`'elec` | Charge of electron, C
`'e0` | Permittivity of vacuum, F/m
`'amu` | Atomic mass unit, kg
`'Na` | Avogadro's number, mol^-1
`'atm` | Standard atmosphere, Pa
`'8` | Unsigned 8-bit max
`'16` | Unsigned 16-bit max
`'24` | Unsigned 24-bit max
`'32` | Unsigned 32-bit max
`'64` | Unsigned 64-bit max
`'kb` | 1 kiB in bytes
`'mb` | 1 MiB in bytes
`'gb` | 1 GiB in bytes
`'tb` | 1 TiB in bytes
 | **==LERPN==**
`'sh` | run shell command, $N = number
`'help` | display help
`'exc` | display the latest exception
`'repr` | debug: repr(x)
`'eval` | debug: eval(x)
`'str` | debug: stringify
`'prec` | set or query precision in digits
`'wfrac` | whole+fraction display mode
`'auto` | automatic display mode
`'eng` | engineering mode, sig figs = 7 or command argument
`'sci` | scientific mode
`'fix` | fixed mode, digits = 6 or command argument
`'nat` | natural mode
`'all` | all digits mode
`'hex` | hexadecimal display mode
`'dec` | decimal display mode
`'oct` | octal display mode
`'bin` | binary display mode
`'dur` | duration display mode

