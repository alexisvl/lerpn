# lerpn - Linux Engineering RPN calculator

It's a curses RPN calculator written in straight Python. Not actually Linux-dependent,
despite the (old) name. Requires Python 3.6 at least.

To install, clone this repository and run `python3 setup.py install --user`.
To test-drive, call `python3 -m LerpnApp` from the root of the source tree.

# Dependencies

Just Python.

# Hacking - adding commands

If you want to add your own commands to lerpn, see
[`HACKING.md`](https://gitlab.com/alexisvl/lerpn/-/blob/trunk/HACKING.md)
for a quick intro.

# Quick usage notes:

How to use lerpn should be obvious for those familiar with RPN. The basic commands:

```
+ - * /

_         negative prefix (avoids collision with - for minus)
enter     dup
x         exchange/swap
bksp      drop
?         display help
ctrl-C    exit
```

For a quick online command reference, see
[`COMMANDS.md`](https://gitlab.com/alexisvl/lerpn/-/blob/trunk/COMMANDS.md).

Single-key commands may be used directly if no number is being entered. During
number entry, symbolic commands work directly, but alphabeticals require holding
Alt (otherwise they're part of the number; see "Advanced input").

Less-used commands begin with a single-quote, then the command name, then
'enter'. For example, the sequence of keys used to compute the sine of 3.14
would be:

```
  3 . 1 4 ' s i n <enter>
```

## Advanced input

Numbers can be input in many formats:

|                       | Format            | Example        | Corresponding display mode |
|-----------------------|-------------------|----------------|----------------------------|
| Tagged value          | `<value>"<tag>`   | `3.14"pi`      |                            |
| Duration              | `day:h:m:s`       | `4:20` (4m20s) | `'dur`                    |
|                       |                   | `1:::` (1day)  |                           |
| Complex               | `<re> j <im>`     | `2 j 3`, `2j3` |                           |
| Fraction              | `num,den`         | `1,2` (½)      |                           |
|                       | `whole,num,den`   | `1,1,4` (1¼)   | `'wfrac`                  |
| Scientific notation   | `<coef>e<mag>`    | `2.3e-24`      | `'sci`                    |
| Engineering notation  | `<num><prefix>`   | `2.3k` (2300)  | `'eng`                    |
|                       |                   | `4u7` (4.7e-6) |                           |
| Radix notation        | 0b<binary>        | `0b10010101`   | `'bin`                    |
|                       | 0o<octal>         | `0o4755`       | `'oct`                    |
|                       | 0d<decimal>       | `0d12345678`   | `'dec`                    |
|                       | 0x<hexadecimal>   | `0xCAFEFACE`   | `'hex`                    |

In all modes, use `_` instead of `-` for negatives (to avoid conflict with the
subtraction operator). `-` is permitted after `e`.

## Display modes

The following display modes are available:

| Mode      | |
|-----------|-|
| `'auto`   | Chooses between `'dur`, `'sci`, `'eng`, `'bin`, `'oct`, `'dec` and `'hex` based on the last used input style. |
| `'eng`    | Displays in engineering notation, using metrix prefixes like `8.2 k`. Use `'eng N` to specify precision. |
| `'sci`    | Displays in scientific notation, like `8.2e3`. Use `'sci N` to specify precision. |
| `'fix`    | Displays in fixed-point, like `8200.000000`. Use `'fix N` for N places. |
| `'nat`    | Displays in natural notation, like `8200`. |
| `'all`    | Displays numbers to full internal precision. |
| `'hex`    | Displays integers in hexadecimal. |
| `'dec`    | Displays integers in decimal. |
| `'oct`    | Displays integers in octal. |
| `'bin`    | Displays integers in binary. |
| `'dur`    | Displays numbers (of seconds) as durations in days, hours, minutes, and seconds. |

## Tags

You can tag numbers to help remember what they are. There are two ways to do
this. One is to enter the number with the tag, using a double-quote to
delimit:

```
  3 . 1 4 " t h i s   i s   p i

gives:    3.14 : this is pi
```

Whitespace is acceptable. Alternatively, you can attach a tag to a number
already on the stack by pushing the string itself to the stack, then using
the lowe-case `t` command to attach:

```
  3 . 1 4 <enter>
  " t h i s   i s   p i <enter>
  t
```

This is currently the only application for strings on the stack.

To remove a tag, use upper-case `T`. The tag will disappear - if this was
unintentional, just use lower-case `u` to undo.

Basic math can be done to numbers while tagged, but if you perform a binary
operation on two tagged numbers, lerpn won't know which tag to keep.

## Shell commands

lerpn can run shell commands for you, with the `'sh` command. Here's a completely
useless example:

```
39
'sh dc -e "$N 3+pq"
```

The bottom number on the stack is available as the environment variable `$N`,
which holds the number formatted as a standard float.

The output of the command is regex-searched for anything resembling a float;
the first match is parsed and pushed to the stack. The full output is displayed
in a scrollbox.
