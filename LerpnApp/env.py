# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

"""Global environment

These variables are accessible to the entire lerpn instance.
"""

# Display format.
# One of the subclasses of DisplayMode from nums
FORMAT = None

# Last auto-selected format, for use by AutoMode.
AUTO_FORMAT = None

# sys.exc_info() from the last exception caught
# This is used by the 'exc command to display the backtrace
LAST_ERROR = None

# Curses STDSCR
STDSCR = None

# Function to display a popup
# POPUP(title, text)
POPUP = None
