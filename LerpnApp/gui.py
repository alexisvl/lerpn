# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.popup import Popup
from kivy.uix.scrollview import ScrollView
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.clock import Clock

import time
import decimal
import sys
import string
from . import nums, env
from .commands import SINGLE_KEY_COMMANDS, COMMANDS

class MainWindow(App):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        env.STDSCR = None
        env.POPUP = self.popup
        self.stack = nums.UndoStack()
        self.exception = None
        decimal.getcontext().prec = 50
        self.title = "lerpn"

    def select_entry(self, *args):
        def actor(*args):
            self.entry.focus = True
        Clock.schedule_once(actor, .05)

    def build(self):
        ly = BoxLayout(
            orientation='vertical',
            size_hint=(1,1),
        )

        ly_status = BoxLayout(orientation='horizontal', size_hint=(1,None))
        ly.add_widget(ly_status)

        self.lbl_status_left = Label(text="", halign="left")
        ly_status.add_widget(self.lbl_status_left)
        self.lbl_status_right = Label(text="", halign="right")
        ly_status.add_widget(self.lbl_status_right)

        self.ly_stack = BoxLayout(
            orientation='vertical',
            size_hint=(1,1),
        )
        ly.add_widget(self.ly_stack)

        self.entry = TextInput(
            multiline=False,
            size_hint_y=None,
            height="40dp",
        )
        self.entry.focus = True
        self.entry.focus_next = self.entry
        self.entry.bind(on_text_validate=self.entry_enter)
        self.entry.old_insert_text = self.entry.insert_text
        self.entry.insert_text = self.entry_edited
        self.entry.old_keyboard_on_key_down = self.entry.keyboard_on_key_down
        self.entry.keyboard_on_key_down = self.entry_key
        ly.add_widget(self.entry)

        self.redraw()
        return ly

    def redraw(self):
        if self.exception is not None:
            env.LAST_ERROR = self.exception
            self.lbl_status_left.text = f"ERROR: {self.exception[1]}"
            self.lbl_status_right.text = ""
            self.exception = None
        else:
            self.lbl_status_left.text = env.FORMAT.name()
            self.lbl_status_right.text = env.FORMAT.help()

        ch = self.ly_stack.children[:]

        for i in ch:
            self.ly_stack.remove_widget(i)

        del self.ly_stack.children[:]

        # Top spacer
        self.ly_stack.add_widget(Label())

        for n, i in enumerate(self.stack):
            rendered = env.FORMAT.format(i)
            lbl = Label(
                text=f"{n}: {rendered}",
                size_hint=(1, None),
                height="40dp",
                halign="left",
            )
            self.ly_stack.add_widget(lbl)

        self.select_entry()

    def entry_key(self, wid, keycode, text, modifiers):
        if keycode[1] == "backspace" and self.entry.text == "":
            self.exec_command(SINGLE_KEY_COMMANDS["\x08"], "")
            return True
        else:
            self.entry.old_keyboard_on_key_down(wid, keycode, text, modifiers)

    def entry_enter(self, wid):
        text = wid.text
        wid.text = ""

        if text == "":
            # Enter pressed
            self.exec_command(SINGLE_KEY_COMMANDS["\n"], "")
            return

        # Check for long commands
        found_command = None
        pre_command = text
        if "'" in text:
            pre, delim, post = text.partition("'")
            if '"' not in pre:
                found_command = delim + post
                pre_command = pre

        if pre_command.strip():
            try:
                parsed = nums.num_parser(pre_command, set_auto=True)
            except Exception as e:
                self.exception = sys.exc_info()
            else:
                self.stack.undopush()
                self.stack.append(parsed)
                self.redraw()

        if found_command is not None:
            cmd, _, arg = found_command.partition(" ")
            if cmd in COMMANDS:
                self.exec_command(COMMANDS[cmd], arg.lstrip())
            else:
                try:
                    raise KeyError(f"Command not found: {cmd}")
                except KeyError:
                    LAST_ERROR = sys.exc_info()

    def entry_edited(self, substring, from_undo=False):
        text = self.entry.text
        if not text and not substring:
            return

        cmdblock = (
            '"' in text or (
                text and (
                    substring[0:1] in string.ascii_letters
                    or (substring[0:1] == "-" and text[-1] in "eEj")
                )
            )
        )
        if cmdblock:
            # Don't interpret anything inside a string, tag, or long cmd
            self.entry.text += substring
            return
        if SINGLE_KEY_COMMANDS.get(substring) is not None:
            cmd = SINGLE_KEY_COMMANDS[substring]
            if text:
                self.entry_enter(self.entry)
            self.exec_command(cmd)
        else:
            self.entry.text += substring

    def exec_command(self, cmd, arg=""):
        old_len = len(self.stack)
        try:
            self.stack.undopush()
            cmd(self.stack, arg)
        except Exception as e:
            self.stack.undo()
            self.exception = sys.exc_info()

        self.redraw()

    def show_exc(self):
        self.popup("Error", str(sys.exc_info()[1]))

    def popup(self, title, text):
        if isinstance(text, list):
            text = "\n".join(text)

        ly = BoxLayout(
            orientation="vertical",
        )
        popup = Popup(
            title=title,
            content=ly,
            size_hint=(0.9, 0.9),
        )

        sv = ScrollView(
            effect_cls="ScrollEffect",
        )
        ly.add_widget(sv)

        lbl = Label(
            text=text,
            valign="top",
            size_hint=(None, None),
            font_name="DroidSansMono",
        )
        lbl.bind(texture_size=lbl.setter("size"))
        sv.add_widget(lbl)

        btn = Button(
            text="Dismiss",
            on_press=popup.dismiss,
            height="40dp",
            size_hint_y=None,
        )
        ly.add_widget(btn)

        popup.bind(on_dismiss=self.select_entry)
        popup.open()
        self.select_entry()

def gui_main():
    MainWindow().run()
