# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

"""Extra mathematical functions."""

import cmath
import math
from decimal import Decimal, getcontext
from fractions import Fraction

from .scalar import *

def dec_divmod(a, b):
    result = a % b
    if result < Decimal(0):
        return a//b - 1, result + b
    else:
        return a//b, result

def frac_divmod(a, b):
    result = a % b
    if result < 0:
        return Fraction(a//b) - 1, result + b
    else:
        return Fraction(a//b), result

def degree_trig(func):
    """Create a unary trig operator that accepts degrees."""
    def wrapper(value):
        """Wrapper function: trig(degrees)"""
        if isinstance(value, Decimal):
            value_deg = value * Decimal(math.pi / 180)
        else:
            value_deg = value * math.pi / 180
        return func(value_deg)
    return wrapper

def degree_invtrig(func):
    """Create a unary inverse trig operator that returns degrees.
    """
    def wrapper(value):
        """Wrapper function: rad2deg(trig(value))"""
        if isinstance(value, (Decimal, Scalar)):
            return func(value) * 180 / Decimal(math.pi)
        else:
            return func(value) * 180 / math.pi
    return wrapper

def autoc(fname):
    """Return a wrapper function that tries to call the right math operation
    by name:

        - When class.{fname} exists, call it
        - For Decimal and DecimalScalar, try math.{fname} if above fails
        - For Fraction and FractionScalar, convert to Decimal(/Scalar) first
    """

    def wrapper(n, *args):
        """Wrapper function: math.fname(float), cmath.fname(complex)"""
        if hasattr(n.__class__, fname):
            return getattr(n.__class__, fname)(n, *args)
        elif isinstance(n, Decimal):
            if hasattr(Decimal, fname):
                return getattr(Decimal, fname)(n, *args)
            else:
                return Decimal(getattr(math, fname)(n, *args))
        elif isinstance(n, Scalar):
            return type(n)(wrapper(n.v, *args))
        elif isinstance(n, Fraction):
            dec = Decimal(n.numerator) / Decimal(n.denominator)
            return Decimal(getattr(math, fname)(dec, *args))
        else:
            raise TypeError("Unsupported type for %s: %r" % (fname, n))
    return wrapper

def clamp(lower, n, upper):
    """Return n clamped to the range lower <= n <= upper"""

    return max(lower, min(n, upper))

def decgamma(x):
    """Compute gamma(x), using exact decimal arithmetic when appropriate"""
    if isinstance(x, (Decimal, DecimalScalar)):
        if int(x) == x and 1 < x < 171:
            product = DecimalScalar("1")
            for i in range(2, int(x)):
                product *= DecimalScalar(i)
            return product
        else:
            return DecimalScalar(math.gamma(float(x)))
    else:
        return math.gamma(x)

def decpi(memo={}):
    """Compute Pi to the current precision.

    >>> print pi()
    3.141592653589793238462643383

    Taken from the Python Decimal documentation and memoized.
    """
    prec = getcontext().prec
    pi = memo.get(prec, None)

    if pi is not None:
        return pi

    getcontext().prec += 2  # extra digits for intermediate steps
    three = Decimal(3)      # substitute "three=3.0" for regular floats
    lasts, t, s, n, na, d, da = 0, three, 3, 1, 0, 0, 24
    while s != lasts:
        lasts = s
        n, na = n+na, na+8
        d, da = d+da, da+32
        t = (t * n) / d
        s += t
    getcontext().prec -= 2
    pi = +s                 # unary plus applies the new precision

    pi = DecimalScalar(pi)
    memo[prec] = pi
    return pi
