# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

"""Scalar number"""

import decimal
import fractions

from decimal import Decimal
from fractions import Fraction
from collections.abc import Iterable

def dec_divmod(a, b):
    result = a % b
    if result < Decimal(0):
        return a//b - 1, result + b
    else:
        return a//b, result

def frac_divmod(a, b):
    result = a % b
    if result < 0:
        return Fraction(a//b) - 1, result + b
    else:
        return Fraction(a//b), result

class Scalar():
    @classmethod
    def coerce(cls, lhs, rhs):
        """Coerce lhs and rhs into compatible types for binary operations.
        Returns (new_lhs, new_rhs).
        """

        from .comflex import Comflex

        if type(lhs) is type(rhs):
            return (lhs, rhs)
        elif isinstance(lhs, DecimalScalar) and isinstance(rhs, FractionScalar):
            new_rhs, new_lhs = cls.coerce(rhs, lhs) # reuse next branch
            return new_lhs, new_rhs
        elif isinstance(lhs, FractionScalar) and isinstance(rhs, DecimalScalar):
            lhs_dec = lhs.to_decimal_scalar()
            rhs_frac = rhs.to_fraction_scalar()

            lhs_err = 0 if lhs.v == 0 else (
                abs(float(lhs_dec) - float(lhs.v))/abs(float(lhs.v))
            )
            rhs_err = 0 if rhs.v == 0 else (
                abs(float(rhs_frac) - float(rhs.v))/abs(float(rhs.v))
            )

            if lhs_err < rhs_err:
                return DecimalScalar(lhs_dec), rhs
            else:
                return lhs, FractionScalar(rhs_frac)
        elif isinstance(lhs, DecimalScalar) and isinstance(rhs, (Decimal, float, int)):
            return lhs, DecimalScalar(rhs)
        elif isinstance(lhs, FractionScalar) and isinstance(rhs, (Fraction, float, int)):
            return lhs, FractionScalar(rhs)
        elif isinstance(lhs, DecimalScalar) and isinstance(rhs, Fraction):
            return cls.coerce(lhs, FractionScalar(rhs))
        elif isinstance(lhs, FractionScalar) and isinstance(rhs, Decimal):
            return cls.coerce(lhs, DecimalScalar(rhs))
        elif isinstance(lhs, Scalar) and isinstance(rhs, Comflex):
            return Comflex(lhs), rhs
        elif isinstance(lhs, Comflex) and isinstance(rhs, Scalar):
            return lhs, Comflex(rhs)
        else:
            raise TypeError(f"don't know how to coerce {lhs!r} and {rhs!r}")

    @staticmethod
    def create_operators(cls):
        for name, unpack, coerce_result, operation in [
                ("__eq__",          True,  False, lambda lhs, rhs: lhs == rhs),
                ("__lt__",          True,  False, lambda lhs, rhs: lhs < rhs),
                ("__gt__",          True,  False, lambda lhs, rhs: lhs > rhs),
                ("__le__",          True,  False, lambda lhs, rhs: lhs <= rhs),
                ("__ge__",          True,  False, lambda lhs, rhs: lhs >= rhs),
                ("__add__",         True,  True,  lambda lhs, rhs: lhs + rhs),
                ("__radd__",        True,  True,  lambda lhs, rhs: rhs + lhs),
                ("__sub__",         True,  True,  lambda lhs, rhs: lhs - rhs),
                ("__rsub__",        True,  True,  lambda lhs, rhs: rhs - lhs),
                ("__mul__",         True,  True,  lambda lhs, rhs: lhs * rhs),
                ("__rmul__",        True,  True,  lambda lhs, rhs: rhs * lhs),
                ("__pow__",         True,  True,  lambda lhs, rhs: lhs ** rhs),
                ("__truediv__",     True,  True,  lambda lhs, rhs: lhs / rhs),
                ("__rtruediv__",    True,  True,  lambda lhs, rhs: rhs / lhs),
                ("__floordiv__",    False, True,  lambda lhs, rhs: lhs.coerced_divmod(rhs)[0]),
                ("__rfloordiv__",   False, True,  lambda lhs, rhs: rhs.coerced_divmod(lhs)[0]),
                ("__mod__",         False, True,  lambda lhs, rhs: lhs.coerced_divmod(rhs)[1]),
                ("__divmod__",      False, True,  lambda lhs, rhs: lhs.coerced_divmod(rhs)),
                ("__and__",         False, True,  lambda lhs, rhs: int(lhs) & int(rhs)),
                ("__or__",          False, True,  lambda lhs, rhs: int(lhs) | int(rhs)),
                ("__xor__",         False, True,  lambda lhs, rhs: int(lhs) ^ int(rhs)),
            ]:

            def get_op(name, unpack, operation, coerce_result):
                if unpack:
                    def op(self, other):
                        lhs, rhs = Scalar.coerce(self, other)
                        if coerce_result:
                            return type(lhs)(operation(lhs.v, rhs.v))
                        else:
                            return operation(lhs.v, rhs.v)
                    return op
                else:
                    def op(self, other):
                        lhs, rhs = Scalar.coerce(self, other)
                        result = operation(lhs, rhs)
                        if isinstance(result, Iterable) and coerce_result:
                            return type(result)(type(lhs)(i) for i in result)
                        elif coerce_result:
                            return type(lhs)(result)
                        else:
                            return result
                    return op

            setattr(cls, name, get_op(name, unpack, operation, coerce_result))

        return cls

@Scalar.create_operators
class DecimalScalar(Scalar):
    def __init__(self, *args, **kwargs):
        if args and isinstance(args[0], DecimalScalar):
            self.v = args[0].v
        else:
            self.v = Decimal(*args, **kwargs)

    def __int__(self):
        return int(
            self.v.to_integral_exact(rounding=decimal.ROUND_DOWN)
        )

    def __float__(self):
        return float(self.v)

    def coerced_divmod(self, other):
        quo, rem = dec_divmod(self.v, other.v)
        return DecimalScalar(quo), DecimalScalar(rem)

    def to_fraction_scalar(self):
        return FractionScalar(Fraction.from_decimal(self.v).limit_denominator(1000000))

    def to_decimal_scalar(self):
        return self

    def __str__(self):
        return str(self.v)

    def __repr__(self):
        return f"DecimalScalar({self.v!r})"

    def __abs__(self):
        return DecimalScalar(abs(self.v))

    def __neg__(self):
        return DecimalScalar(-self.v)

    def exp(self):
        return DecimalScalar(self.v.exp())

    def log10(self):
        return DecimalScalar(self.v.log10())

    def normalize(self):
        return DecimalScalar(self.v.normalize())

    def as_tuple(self):
        return self.v.as_tuple()

    def is_signed(self):
        return self.v.is_signed()

@Scalar.create_operators
class FractionScalar(Scalar):
    def __init__(self, *args, **kwargs):
        if args and isinstance(args[0], FractionScalar):
            self.v = args[0].v
        else:
            self.v = Fraction(*args, **kwargs)

    def __int__(self):
        num, den = self.v.numerator, self.v.denominator

        if num < 0:
            num = -num
            neg = True
        else:
            neg = False

        num -= (num % den)

        if neg:
            num = -num

        return int(num // den)

    def __float__(self):
        return float(self.v)

    def to_fraction_scalar(self):
        return self

    def to_decimal_scalar(self):
        return DecimalScalar(
            Decimal(self.v.numerator) / Decimal(self.v.denominator)
        )

    def coerced_divmod(self, other):
        quo, rem = frac_divmod(self.v, other.v)
        return FractionScalar(quo), FractionScalar(rem)

    def __str__(self):
        return str(self.v)

    def __repr__(self):
        return f"FractionScalar({self.v!r})"

    def __abs__(self):
        return FractionScalar(abs(self.v))

    def __neg__(self):
        return FractionScalar(-self.v)

    def exp(self):
        return DecimalScalar(self.to_decimal_scalar().v.exp())

    def log10(self):
        return DecimalScalar(self.to_decimal_scalar().v.log10())

    def normalize(self):
        return self

    def is_signed(self):
        return self.v < 0

    @classmethod
    def from_decimal(cls, dec):
        if isinstance(dec, DecimalScalar):
            dec = dec.v
        return cls(Fraction.from_decimal(dec))
