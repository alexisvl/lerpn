# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

"""lerpn application module

To run lerpn, call main().
"""

__version__ = "5"

import curses
import os
import signal
import sys
import decimal
from . import nums, env, tui

def wrapped_main(stdscr):
    """main function to be wrapped by curses.wrapper
    """

    def curses_popup(title, text):
        scrollbox = curses_stuff.CursesScrollBox(env.STDSCR, -4, -4)
        scrollbox.set_title(title)
        scrollbox.set_text(text)
        scrollbox.show()

    curses.nonl()
    env.STDSCR = stdscr
    env.POPUP = curses_popup
    stack = nums.UndoStack()
    prompt = tui.Prompt()
    decimal.getcontext().prec = 50

    # Register exit handler
    def exit_handler(signum, frame):
        """Call the lerpn cleanup/exit function, to be used as a signal handler"""
        tui.do_exit(stack)

    signal.signal(signal.SIGINT, exit_handler)

    while not prompt.quit:
        stdscr.clear()
        prompt.loop(stack)

    os.kill(os.getpid(), signal.SIGINT)

def main():
    """call this to launch lerpn
    """
    debug = os.getenv("DEBUG_LERPN")
    if debug is not None and int(debug):
        import rpdb
        rpdb.set_trace()

    if "--help" in sys.argv or "-h" in sys.argv:
        print("usage: %s [--version] [--help]" % os.path.basename(sys.argv[0]))
        print("")
        print("lerpn (Linux Engineering RPN calculator)")
        print("Run with no arguments to start the calculator. Once started, try")
        print("pressing ? to see a list of commands.")
        return

    if "--version" in sys.argv:
        print("lerpn version " + __version__)
        return

    if "--gui" in sys.argv:
        # kivy cries on import...
        sys.argv.remove("--gui")
        from . import gui
        gui.gui_main()
    else:
        curses.wrapper(wrapped_main)

def gen_commands_md():
    """generate a Markdown reference of all commands"""

    from . import commands, curses_stuff

    print("# Quick Command Reference")
    print()
    print("This list is auto-generated from the code, and contains the same")
    print("text you would see in the in-program help box.")
    print()
    print("## Single-key commands")
    print()
    print("Key | Description")
    print("--- | -----------")
    for cmd in commands.SINGLE_KEY_COMMANDS:
        cmd_name = curses_stuff.KEY_TO_NAME.get(cmd, "`%s`" % cmd)
        cmd_function = commands.SINGLE_KEY_COMMANDS[cmd]
        if cmd_function is None or cmd_name is None:
            continue
        cmd_desc = cmd_function.__doc__.partition("\n")[0]
        print("%s | %s" % (cmd_name, cmd_desc))
    print()

    print("## Long commands")
    print()
    print("Name | Description")
    print("---- | -----------")
    for cmd in commands.COMMANDS:
        if isinstance(cmd, int):
            cmd_name = ""
            cmd_desc = "**%s**" % commands.COMMANDS[cmd]
        else:
            cmd_name = "`%s`" % cmd
            cmd_desc = commands.COMMANDS[cmd].__doc__.partition("\n")[0]
        print("%s | %s" % (cmd_name, cmd_desc))
    print()

if __name__ == "__main__":
    main()
