# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

"""Code dealing with numbers.
"""

import re
import math
from decimal import Decimal
from fractions import Fraction
import decimal
import string
from functools import reduce
from .scalar import *
from .comflex import *
from .extramath import *
from . import env

###############################################################################
# NUMBER FORMAT
#
# Numbers are parsed recursively, with the outer type determining the final
# type of the expression (internals are resolved to float/complex).
#
# Formats recognized, in order of resolution:
#
# Tag           "-delimited fields, up to 2
#                   a"b         a with tag "b"
#
# Duration      colon-delimited fields, rightmost is seconds
#                   4:20        4 minutes, 20 seconds
#                   4:20:       4 hours, 20 minutes
#                   4:20:69     4 hours, 20 minutes, 69 seconds
#                   1::         1 hour
#                   1:::        1 day
#
# Complex       j-delimited fields, up to 2
#                   1j2         (1+2j)
#
# Fraction      comma-delimited fields, up to 3
#                   1,2         (1/2)
#                   3,1,2       3+(1/2) or (3/2)
#
# Numeric sci   e-delimited fields, up to 2
#                   1e5         1 * 10^5
#
# Radix         prefix of 0b, 0o, 0d, 0x
#
# Metric sci    single-letter suffix, one of:
#                   yzafpnµumkMGTPEZY
#
# Negative      prefix of _ or -

PREFIXES = "yzafpnµumkMGTPEZY"

def num_parser(s, set_auto=False):
    s = s.strip()
    if s == "":
        return DecimalScalar(0)
    elif '"' in s:
        val, delim, tag = s.partition('"')
        return Tagged(
            num_parser(val),
            tag.strip(),
        )
    elif ":" in s:
        fields = s.split(":")
        accum = DecimalScalar(0)
        multipliers = [1, 60, 3600, 86400]
        if len(fields) > 4:
            return ValueError("Too many fields in :-delimited value (d:h:m:s)")
        for i, mult in zip(fields[::-1], multipliers[:len(fields)]):
            i_val = num_parser(i)
            if isinstance(i_val, Comflex):
                # siiiiiiigh
                accum = Comflex(accum)
            accum += mult * i_val

        if set_auto:
            env.AUTO_FORMAT = TimeDurationMode()

        return accum
    elif "j" in s:
        realp, delim, cplxp = s.partition("j")
        return Comflex(
            num_parser(realp),
            num_parser(cplxp),
        )
    elif "," in s:
        fields = s.split(",")
        if len(fields) < 2 or len(fields) > 3:
            return ValueError("Too many commas in fraction (num,dec or whole,num,dec)")

        if fields[0].lstrip()[0:1] in ("-", "_"):
            sign = -1
            fields[0] = fields[0].lstrip()[1:]
        else:
            sign = 1

        if len(fields) == 3:
            wpart = num_parser(fields[0])
            fields = fields[1:]
        else:
            wpart = DecimalScalar(0)

        num = num_parser(fields[0])
        den = num_parser(fields[1])

        # Hack to handle non-integer num/den by simplification
        return sign * (
            FractionScalar.from_decimal(wpart)
            + FractionScalar.from_decimal(num)
              / FractionScalar.from_decimal(den)
        )
    elif s[0:2] in ("0b", "0o", "0d", "0x"):
        radix, mode = {
            "0b": (2, BinMode),
            "0o": (8, OctMode),
            "0d": (10, DecMode),
            "0x": (16, HexMode),
        }[s[0:2]]
        val = DecimalScalar(int(s[2:], radix))
        if set_auto:
            env.AUTO_FORMAT = mode()
        return val
    elif "e" in s:
        coef, delim, mag = s.partition("e")
        coef_val = num_parser(coef)
        mag_val = num_parser(mag)

        if set_auto:
            env.AUTO_FORMAT = SciMode()

        if any(isinstance(i, Comflex) for i in (coef_val, mag_val)):
            # siiiiiiigh
            return (
                Comflex(coef_val)
                * (Comflex(10) ** Comflex(mag_val))
            )
        else:
            return coef_val * (DecimalScalar(10) ** mag_val)
    elif any(i in s for i in PREFIXES):
        for ch in s[::-1]:
            if ch in PREFIXES:
                prefix = ch
                break

        coef = s.replace(prefix, ".").rstrip(".")
        coef_val = num_parser(coef)

        if set_auto:
            env.AUTO_FORMAT = EngMode()

        if isinstance(coef_val, Comflex):
            ten = Comflex(10)
        else:
            ten = DecimalScalar(10)
        return coef_val * (ten ** {
            "y": -24, "z": -21, "a": -18, "f": -15, "p": -12,
            "n": -9, "µ": -6, "u": -6, "m": -3, "k": 3, "M": 6,
            "G": 9, "T": 12, "P": 15, "E": 18, "Z": 21, "Y": 24,
        }[prefix])
    elif s[0] == "_":
        return -num_parser(s[1:])
    else:
        return DecimalScalar(s)

LUT_UNICODE = {
    -24: "y", -21: "z", -18: "a", -15: "f", -12: "p",
    -9: "n", -6: u"\xb5", -3: "m", 0: "", 3: "k",
    6: "M", 9: "G", 12: "T", 15: "P", 18: "E",
    21: "Z", 24: "Y"
}

LUT_ASCII = {
    -24: "y", -21: "z", -18: "a", -15: "f", -12: "p",
    -9: "n", -6: "u", -3: "m", 0: "", 3: "k",
    6: "M", 9: "G", 12: "T", 15: "P", 18: "E",
    21: "Z", 24: "Y"
}

import curses
if hasattr(curses, "unget_wch"):
    LUT = LUT_UNICODE
else:
    LUT = LUT_ASCII

REVERSE_LUT = {v:k for k,v in LUT_ASCII.items()}

ALL_PREFIXES = "".join(REVERSE_LUT.keys())

FLOAT_RE = re.compile(r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+|[_ ]?["+ALL_PREFIXES+"])?")
FLOAT_RE_UNDER = re.compile(r"[-_+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+|[_ ]["+ALL_PREFIXES+"])?")

# This regex handles both complex numbers and times. Yes, it could be used to
# input nonsense like 4 days + 3j.
NUM_PARSE_RE = re.compile(
    r"\s*(?P<days>" + FLOAT_RE_UNDER.pattern + r"[dD])?" +     # days
    r"\s*(?P<hours>" + FLOAT_RE_UNDER.pattern + r"[hH])?" +     # hours
    r"\s*(?P<mins>" + FLOAT_RE_UNDER.pattern + r"[mM])?" +     # minutes
    r"\s*(?P<re>" + FLOAT_RE_UNDER.pattern + r")?[sS]?" +     # real part
    r"\s*(?P<im>[jJ]\s*" + FLOAT_RE_UNDER.pattern + ")?" + # imaginary part
    r'\s*(?P<tag>".*)?' +                               # tag
    "$")

# number with radix
RNUM_PARSE_RE = re.compile(
    r"\s*(?P<radix>0[bdohx])" +                         # radix
    r"\s*(?P<re>[-+]?[0-9A-Fa-f]+)" +                   # real part
    r'\s*(?P<tag>".*)?' +                               # tag
    "$")

class UndoStack(list):
    """Stack supporting an 'undo' action.
    This is used as the main RPN stack. You can append() and pop() and [] just
    like with a normal list. You can also use undopush() to push a duplicate of
    the stack itself onto a "stack of stacks", and then undo() to restore that.

    undo() moves the last item to the redo stack. Use drop() to drop it without
    doing this.
    """

    def __init__(self, v=None):
        """Initialize an UndoStack from an optional source list.
        """
        self.__redo_held = None
        if v is None:
            list.__init__(self)
            self.__stack = [[]]
            self.__redo = []

        else:
            list.__init__(self, v)
            self.__stack = [v[:]]
            self.__redo = []

        self.parent = None

    def __repr__(self):
        return 'UndoStack(%r)' % (list(self))

    def undopush(self):
        """Save the current stack state to be restored later with undo()
        """
        self.__redo_held = self.__redo
        self.__redo = []
        self.__stack.append(self[:])

    def undo(self):
        """Restore the last saved undo state"""
        self.__redo.append(self[:])
        self.drop()

    def drop(self):
        """Restore the last saved undo state, but do not push the current
        state to the redo stack.
        """
        if len(self.__stack) > 1:
            self[:] = self.__stack.pop()

    def redo(self):
        """Undo the last undo"""
        if self.__redo:
            self[:] = self.__redo.pop()

    def replace_redo(self):
        """replace the redo stack directly after undopush has cleared it."""
        self.__redo = self.__redo_held

class Tagged():
    """Tagged number object.
    This behaves like a number, but also contains a string tag. The values
    are accessible at .num and .tag
    """
    def __init__(self, *args):
        if len(args) == 2:
            num, tag = args
            assert isinstance(tag, str)
            self._num = num
            self._tag = tag
        elif len(args) == 1:
            assert isinstance(args[0], Tagged)
            self._num = type(args[0]._num)(args[0]._num)
            self._tag = args[0]._tag
        else:
            raise TypeError("Invalid arguments to Tagged()")

    @property
    def num(self):
        """the number that is tagged"""
        return self._num

    @property
    def tag(self):
        """the tag applied to the number"""
        return self._tag

    def __repr__(self):
        return "Tagged(%r, %r)" % (self._num, self._tag)

    @classmethod
    def generate_operators(cls):
        """Autogenerates a set of operator overloads"""
        # Add operations to Tagged
        ops = [
            ("add", lambda x, y: x+y),
            ("sub", lambda x, y: x-y),
            ("mul", lambda x, y: x*y),
            ("div", lambda x, y: x/y),
            ("truediv", lambda x, y: x/y),
            ("mod", lambda x, y: x%y),
            ("pow", lambda x, y: x**y),
            ]
        for opname, opfunc in ops:
            # For the uninitiated, the default-argument parameters create a new
            # scope for the variable, allowing passing each loop iteration's value
            # to the closure instead of closing around the single final value.
            def method(self, other, opfunc=opfunc):
                """automatically defined operator"""
                if isinstance(other, cls):
                    # If both are tagged, just remove the tag.
                    return opfunc(self.num, other.num)
                if not isinstance(other, (Decimal, Scalar)) and not isinstance(other, Comflex):
                    return NotImplemented
                return cls(opfunc(self.num, other), self.tag)

            def rmethod(self, other, opfunc=opfunc):
                """automatically defined operator"""
                if isinstance(other, cls):
                    # If both are tagged, just remove the tag.
                    return opfunc(other.num, self.num)
                if not isinstance(other, (Decimal, Scalar)) and not isinstance(other, Comflex):
                    return NotImplemented
                return cls(opfunc(other, self.num), self.tag)

            setattr(cls, "__%s__" % opname, method)
            setattr(cls, "__r%s__" % opname, rmethod)

Tagged.generate_operators()

def eng(num, sigfigs=7):
    """Return num in engineering notation"""

    if isinstance(num, Comflex):
        if all(isinstance(i, DecimalScalar) for i in (num.real, num.imag)):
            real = num.real
            imag = num.imag
            imag_sign = "" if imag >= Decimal("0") else "-"
            imag_abs = imag if imag >= Decimal("0") else -imag

            return "%s j %s%s" %(eng(real), imag_sign, eng(imag_abs))
        else:
            real = num.real.to_decimal_scalar()
            imag = num.imag.to_decimal_scalar()
            return eng(Comflex(real, imag), sigfigs=sigfigs)

    rounded, inexact = round_dec_sigfigs(num, sigfigs)
    sign, digits, exponent = rounded.as_tuple()
    if exponent == 'F':
        return 'inf' if sign == '1' else '-inf'
    exponent += len(digits) - 1

    if len(digits) < sigfigs:
        digits += (sigfigs - len(digits)) * (0,)

    n_digits_left = exponent % 3 + 1
    exponent -= n_digits_left - 1

    digits_left = ''.join(str(d) for d in digits[:n_digits_left])
    digits_right = ''.join(str(d) for d in digits[n_digits_left:sigfigs]).rstrip("0")
    dot = '.' if (len(digits_right) or inexact) else ''

    exp_prefix = LUT.get(exponent, None)
    if exp_prefix is None:
        exp_text = "e%i" % exponent
    elif exp_prefix == "":
        exp_text = ""
    else:
        exp_text = " " + exp_prefix

    return (('-' if sign else '') +
            digits_left + dot + digits_right +
            exp_text)

def round_dec_nfrac(num, nfrac):
    """Round and normalize a Decimal to a number of fractional digits.

    Returns (rounded, inexact) where `rounded` is the rounded Decimal and
    `inexact` is whether it was modified."""
    sigfigs = nfrac + max(num.adjusted(), 0) + 1
    return round_dec_sigfigs(num, sigfigs)

def round_dec_sigfigs(num, sigfigs):
    """Round and normalize a Decimal to a number of significant figures.

    Returns (rounded, inexact) where `rounded` is the rounded Decimal and
    `inexact` is whether it was modified."""
    num = num.normalize()
    sign, digits, exponent = num.as_tuple()

    if len(digits) < (sigfigs + 1):
        return num, False

    digits = list(digits)

    # Round towards even
    if digits[sigfigs] > 5:
        carry = 1
    elif digits[sigfigs] < 5:
        carry = 0
    else:
        carry = digits[sigfigs-1] & 1

    # Propagate carry
    for i in reversed(range(0, sigfigs)):
        digits[i] += carry
        if digits[i] > 9:
            digits[i] -= 10
            carry = 1
        else:
            carry = 0

    inexact = sum(digits[sigfigs:]) != 0

    digits = [carry] + digits[:sigfigs] + [0] * (len(digits) - sigfigs)

    return Decimal((sign, digits, exponent)).normalize(), inexact

def natfix(num, nfrac, padded=False):
    """Format a Decimal in fixed mode. The number will be truncated to nfrac
    fractional digits, so use the round_dec functions first if this is not
    desirable.

    num: number to format
    nfrac: number of fractional digits following the point
    padded: whether to pad with zero to nfrac
    """

    sign, digits, exponent = num.as_tuple()
    intfigs = max(len(digits) + exponent, 0)
    # Caculate how much the fractional part needs to be padded
    fracpads = max(-exponent-len(digits), 0)
    # Obtain the integer part
    intdigs = list(digits[:intfigs]) or [0]
    # Pad it if it's not enough to cover the exponent
    intdigs += [0] * (intfigs-len(intdigs))
    intpart = reduce(lambda r,d: r*10 + d, intdigs) # Turn it into a real integer
    # Obtain a mutable copy of the fractional part
    fracdigs = list(digits[intfigs:])
    # Left pad if the exponent doesn't cover it
    fracdigs = [0] * (fracpads) + fracdigs
    if padded: # Add right padding if requested
        fracdigs += [0] * (nfrac+1-len(fracdigs))

    fracdigs = fracdigs[:nfrac]
    if intfigs == 0 and intpart == 0 and sum(fracdigs) == 0: # underflow
        s = "-0" if sign else "+0"
        if nfrac:
            s += "." + "0" * nfrac
    else:
        s = ('-' if sign else '') + '%i' % intpart
        if nfrac:
            s += "." + "".join(str(d) for d in fracdigs)
            if not padded:
                s = s.rstrip("0")
    return s

class DisplayMode():
    """Class with methods to implement a display mode"""

    def format_real(self, num):
        """Format a real number (Decimal) according to this display mode.
        Must be implemented for each mode."""
        raise NotImplementedError

    def format_frac(self, num):
        """Format a fraction according to this display mode. Only needs to be
        overridden if desired."""
        numer = Decimal(num.numerator)
        denom = Decimal(num.denominator)
        return self.format_real(numer) + "," + self.format_real(denom)

    def format(self, num):
        """Format the number in the subclass mode. Do not call this directly
        on DisplayMode, only on its subclasses."""
        if isinstance(num, Tagged):
            return self.format(num.num)

        elif isinstance(num, Fraction):
            return self.format_frac(num)

        elif isinstance(num, Scalar):
            return self.format(num.v)

        elif isinstance(num, Decimal):
            return self.format_real(num)

        elif isinstance(num, Comflex):
            real = num.real
            imag = num.imag
            imag_sign = "" if imag >= 0 else "-"
            imag_abs = imag if imag >= 0 else -imag

            return f"{self.format(real)} j {imag_sign}{self.format(imag_abs)}"

        elif isinstance(num, str):
            return repr(num)

        else:
            return str(num)

    def name(self):
        """Return a short name for displaying in the status bar"""
        return str(self)

    def help(self):
        """Return a short help string to display on the right of the bar"""
        return ""

class EngMode(DisplayMode):
    """Engineering mode. Numbers are displayed similarly to scientific notation,
    but with exponents always a multiple of 3, and using metric prefixes:

    13000.  becomes 13 k
    """

    def __init__(self, sigfigs=7):
        self.sigfigs = sigfigs

    def format_real(self, num):
        """Format a Decimal in engineering mode"""
        return eng(num, self.sigfigs)

    def name(self):
        return "'eng %d" % self.sigfigs

class FixMode(DisplayMode):
    """Fixed mode. Numbers are displayed with a fixed number of places after
    the decimal point."""

    def __init__(self, digits=6):
        self.digits = digits

    def format_real(self, num):
        """Format a Decimal in fixed mode"""
        rounded, _ = round_dec_nfrac(num, self.digits)
        return natfix(rounded, self.digits, padded=True)

    def name(self):
        return "'fix %d" % self.digits

class AllMode(DisplayMode):
    """All digits mode. Display the entire value without any truncation or rounding."""

    def format_real(self, num):
        """Display a Decimal with no additional formatting"""
        return str(num)

    def name(self):
        return "'all"

class TimeDurationMode(DisplayMode):
    """Display value as days/hours/minutes/seconds. NaturalMode is used as a
    sub-renderer."""

    def format_real(self, num):
        """Format a Decimal in TimeDurationMode"""

        if num.is_signed():
            return "-" + self.format_real(-num)

        days, hours, minutes = type(num)(0), type(num)(0), type(num)(0)

        secs = num
        if secs >= 86400:
            days, secs = secs // 86400, secs % 86400
        if secs >= 3600:
            hours, secs = secs // 3600, secs % 3600
        if secs >= 60:
            minutes, secs = secs // 60, secs % 60

        nm = NaturalMode()

        days_f = nm.format(days)
        hours_f = nm.format(hours)
        minutes_f = nm.format(minutes)
        secs_f = nm.format(secs)

        fields = [days_f, hours_f, minutes_f, secs_f]

        # Reformat for visual clarity:
        #   - leading zeros are dropped
        #   - trailing <10 values are rendered as two digits
        while fields[0:1] == ["0"]:
            fields.pop(0)
        for n in range(len(fields)):
            if len(fields[n]) == 1 and fields[n][0] in string.digits:
                fields[n] = "0" + fields[n]

        if len(fields) == 0:
            return ":00"
        elif len(fields) == 1:
            return ":" + fields[0]
        else:
            return ":".join(fields)

    def name(self):
        return "'dur"

    def help(self):
        return "d:h:m:s"

class NaturalMode(DisplayMode):
    """Natural mode. Very large or small numbers are displayed in scientific notation;
    midrange numbers are displayed in standard notation."""

    def format_real(self, num, sigfigs=6):
        """Format a Decimal in natural mode"""
        if num.adjusted() >= 6 or num.adjusted() <= -5:
            return SciMode(sigfigs).format_real(num)
        else:
            rounded, inexact = round_dec_sigfigs(num, sigfigs)
            is_int = num.to_integral_value() == num and not inexact
            if is_int:
                return natfix(rounded, 0)
            else:
                return natfix(rounded, sigfigs)

    def name(self):
        return "'nat"

class SciMode(DisplayMode):
    """Scientific mode. All numbers are displayed as as [-]d.ddde+dd"""

    def __init__(self, sigfigs=6):
        self.sigfigs = sigfigs

    def format_real(self, num):
        """Format a Decimal in scientific mode"""
        rounded, _ = round_dec_sigfigs(num, self.sigfigs)
        sign, digits, exponent = rounded.as_tuple()
        exponent += len(digits)-1

        return (('-' if sign else '') +
                str(digits[0]) + '.' +
                ''.join(str(d) for d in digits[1:self.sigfigs]) +
                ("e%i" % exponent))

    def name(self):
        return f"'sci {self.sigfigs}"

class HexMode(DisplayMode):
    """Display integers in hexadecimal, non-integers in natural mode."""
    def format_real(self, num):
        """Format a Decimal, rounded to integer, in hexadecimal"""

        return format_int(num, hex)

    def name(self):
        return "'hex"

class DecMode(DisplayMode):
    """Display integers in decimal, non-integers in natural mode."""
    def format_real(self, num):
        """Format a Decimal, rounded to integer, in decimal"""
        return format_int(num, str)

    @classmethod
    def name(self):
        return "'dec"

class OctMode(DisplayMode):
    """Display integers in octal, non-integers in natural mode."""
    def format_real(self, num):
        """Format a Decimal, rounded to integer, in octal"""

        return format_int(num, oct)

    def name(self):
        return "'oct"

class BinMode(DisplayMode):
    """Display integers in binary, non-integers in natural mode."""
    def format_real(self, num):
        """Format a Decimal, rounded to integer, in binary"""
        return format_int(num, bin)

    def name(self):
        return "'bin"

class AutoMode(DisplayMode):
    """Automatic mode from last input method"""

    def format_real(self, num):
        return env.AUTO_FORMAT.format_real(num)

    def format_frac(self, num):
        return env.AUTO_FORMAT.format_frac(num)

    def name(self):
        return f"'auto (using {env.AUTO_FORMAT.name()})"

    def help(self):
        return env.AUTO_FORMAT.help()

class WfracMode(DisplayMode):
    """Whole+frac mode based on previous mode. Construct with previous mode"""

    def __init__(self, prev):
        self.prev = prev

    def format_frac(self, num):
        if isinstance(num, Fraction) and num.numerator >= num.denominator:
            wpart = num.numerator // num.denominator
            fmt_frac = self.prev.format(
                Fraction(
                    num.numerator - (wpart * num.denominator),
                    num.denominator
                )
            )
            return f"{wpart},{fmt_frac}"
        else:
            return self.prev.format_frac(num)

    def format_real(self, num):
        return self.prev.format_real(num)

    def name(self):
        return f"'wfrac on {self.prev.name()}"

    def help(self):
        return "whole,num,den"

def format_int(value, fxn):
    """If 'value' is equal to an integer, format it as
    fxn(int(value)), otherwise as "%g" % value."""

    if value.to_integral_value() == value:
        return fxn(int(value))
    else:
        return NaturalMode().format_real(value)

env.FORMAT = AutoMode()
env.AUTO_FORMAT = EngMode()
