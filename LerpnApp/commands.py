# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

"""All the calculator commands.

This file contains the commands made available to the user. Single-key
commands are in the SINGLE_KEY_COMMANDS OrderedDict, and full-name
commands are in the COMMANDS OrderedDict.
"""

import cmath
import collections
import curses
import math
import os
import subprocess
import sys
import traceback
from decimal import Decimal
from fractions import Fraction
import decimal

from . import nums, env, curses_stuff
from .extramath import *
from .scalar import *
from .comflex import *

###############################################################################
# COMMAND GENERATORS
#
# Use these to generate common cases, rather than implementing from scratch.

def BINARY(func, docstring=None):
    """Create a binary operator from a lambda function."""
    def cmd(stack, arg):
        y_val = stack.pop()
        x_val = stack.pop()
        stack.append(func(x_val, y_val))

    cmd.__doc__ = docstring or func.__doc__
    assert cmd.__doc__

    return cmd

def UNARY(func, docstring=None):
    """Create a unary operator from a lambda function.
    The unary operators know how to handle tagged numbers - they reapply the
    same tag after performing the operation.
    """
    def cmd_one_value(value):
        if isinstance(value, nums.Tagged):
            newtagged = nums.Tagged(func(value.num), value.tag)
            return newtagged
        else:
            return func(value)

    def cmd(stack, arg):
        stack.append(cmd_one_value(stack.pop()))

    cmd.__doc__ = docstring or func.__doc__
    assert cmd.__doc__

    return cmd

def CONST(val, descr):
    """Return a command function to push a constant.

    The "constant" can be a callable, in which case it will be called at
    command invocation time and replaced with its result. This allows Decimal
    arithmetic to be done under the current context.
    """

    try:
        unicode
    except NameError:
        unicode = str

    if isinstance(val, (float, str, unicode, int, Decimal)):
        val = DecimalScalar(val)
    elif callable(val):
        val = val
    elif not isinstance(val, Comflex):
        raise TypeError("bad type for CONST")

    def cmd(stack, arg):
        if callable(val):
            stack.append(val())
        else:
            stack.append(val)

    cmd.__doc__ = descr
    return cmd

###############################################################################
# COMMAND HELPERS
def untagged(num):
    """return a number without its tag, if it is tagged"""
    if isinstance(num, nums.Tagged):
        return num.num
    else:
        return num

###############################################################################
# MATH COMMANDS

@UNARY
def _f(x):
    """resolve to the nearest fraction"""
    return x.to_fraction_scalar()

@UNARY
def _n(x):
    """resolve to the nearest non-fraction"""
    return x.to_decimal_scalar()

def _divmod(stack, arg):
    """(y/x, y%x)"""
    x = untagged(stack.pop())
    y = untagged(stack.pop())

    if isinstance(x, Decimal) and isinstance(y, Decimal):
        q,r = dec_divmod(y, x)
        stack.append(q)
        stack.append(r)
    elif hasattr(y, "__divmod__"):
        q,r = y.__divmod__(x)
        stack.append(q)
        stack.append(r)
    elif isinstance(x, Comflex) or isinstance(y, Comflex):
        raise TypeError("can't mod complex numbers.")
    else:
        raise TypeError("invalid type for divmod: %s" % type(x))

def _reim(stack, arg):
    """split into real and imaginary parts"""
    num = untagged(stack.pop())
    stack.append(num.real)
    stack.append(num.imag)

def _polar(stack, arg):
    """split into absolute value and angle"""
    num = untagged(stack.pop())

    if isinstance(num, Comflex):
        stack.append(num.abs())
        stack.append(num.arg())
    elif isinstance(num, Decimal):
        if num.is_signed():
            stack.append(abs(num))
            stack.append(decpi())
        else:
            stack.append(num)
            stack.append(Decimal("0"))
    else:
        stack.append(abs(num))
        stack.append(cmath.phase(num))

def _fcart(stack, arg):
    """make complex from Cartesian real, imag"""
    imag = untagged(stack.pop())
    real = untagged(stack.pop())

    stack.append(Comflex(real, imag))

def _fpolar(stack, arg):
    """make complex from polar abs val, angle(rad)"""
    angle = untagged(stack.pop())
    absval = untagged(stack.pop())

    stack.append(Comflex.from_polar(absval, angle))

@UNARY
def _log(x):
    """log base e"""
    if isinstance(x, Decimal):
        return x.ln()
    else:
        return autoc("log")(x)

@BINARY
def _logn(x, y):
    """log base Y"""
    if any(isinstance(i, Comflex) for i in (x, y)):
        decimal.getcontext().prec += 4
        res = Comflex(x).log10() / Comflex(y).log10()
        decimal.getcontext().prec -= 4
        return res.normalize()
    else:
        decimal.getcontext().prec += 4
        res = x.log10() / y.log10()
        decimal.getcontext().prec -= 4
        return res.normalize()

def _log2(stack, arg):
    """log base 2"""
    stack.append(Decimal("2"))
    _logn(stack, arg)

@BINARY
def _unsigned(x, y):
    """Y to unsigned in X-bit"""
    x = int(x)
    y = int(y)
    return Decimal((2 ** y + x) % (2 ** y))

@BINARY
def _signed(x, y):
    """Y to signed in X-bit"""
    x = int(x)
    y = int(y)
    return Decimal(((x - 2**y) + 2**y//2) % 2**y - 2**y//2)

@UNARY
def _conj(x):
    """complex conjugate"""
    if isinstance(x, Comflex):
        return Comflex(x.real, -x.imag)
    else:
        return x

@UNARY
def _abs(x):
    """absolute value"""
    if isinstance(x, Comflex):
        return x.abs()
    else:
        return abs(x)

@UNARY
def _angle(x):
    """angle/phase (rad)"""
    if isinstance(x, Comflex):
        return x.arg()
    else:
        if x.is_signed():
            return decpi()
        else:
            return Decimal(0)

###############################################################################
# TAG COMMANDS

def _tag(stack, arg):
    """attach tag to number"""

    if arg:
        tag = arg.strip()
    else:
        tag = stack.pop()

    num = stack.pop()
    if not isinstance(tag, str):
        raise TypeError("to create tagged number, give Y: number, X: string")

    elif not isinstance(num, (Scalar, Comflex, nums.UndoStack)):
        raise TypeError("to create tagged number, give Y: number, X: string ")

    stack.append(nums.Tagged(num, tag))

def _untag(stack, arg):
    """unTag"""
    stack.append(untagged(stack.pop()))

################################################################################
# STACK/SHELL COMMANDS

def _drop(stack, arg):
    """drop"""
    stack.pop()

def _dup(stack, arg, n=-1):
    """dup"""

    # @param n - which index to dup. This allows _dup to be wrapped by other
    # commands like _get and avoid some duplication.

    if n == -1:
        # pop instead of access so we get the same, consistent error message if
        # there isn't one.
        item = stack.pop(n)
        stack.append(item)
    else:
        item = stack[n]

    stack.append(type(item)(item))

def _rotup(stack, arg):
    """rotate up"""
    stack.append(stack.pop(0))

def _rotdown(stack, arg):
    """rotate down"""
    stack.insert(0, stack.pop(-1))

def _pull(stack, arg):
    """move item from index to front"""
    idx = int(stack.pop())
    stack.append(stack.pop(idx))

def _push(stack, arg):
    """move item from front to index"""
    idx = int(stack.pop())
    stack.insert(idx, stack.pop())

def _undo(stack, arg):
    """undo"""
    # Drop the undopush that was done for /this/ command
    stack.replace_redo()
    stack.drop()
    stack.undo()

def _redo(stack, arg):
    """redo"""
    # Drop the undopush that was done for /this/ command
    stack.replace_redo()
    stack.drop()
    stack.redo()

def _xchg(stack, arg):
    """exchange/swap"""
    x = stack.pop()
    y = stack.pop()
    stack.append(x)
    stack.append(y)

def _get(stack, arg):
    """dup item by number"""
    stack_idx = clamp(0, int(untagged(stack.pop())), len(stack) - 1)
    _dup(stack, arg, n=stack_idx)

def _sh(stack, arg):
    """run shell command, $N = number"""
    assert arg, "argument expected!"
    newenv = dict(os.environ)
    if not stack:
        newenv["N"] = "0"
    else:
        num = untagged(stack[-1])
        if isinstance(num, Comflex):
            newenv["N"] = "%g+%gj" %(num.real, num.imag)
        else:
            newenv["N"] = "%g" % num

    try:
        proc = subprocess.Popen(
            arg, shell=True, env=newenv, stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT)
        data = proc.stdout.read()
        proc.wait()
    except KeyboardInterrupt:
        data = b""

    if sys.version_info >= (3, 0):
        data = data.decode("utf8")

    match = nums.FLOAT_RE.search(data)
    if match is not None:
        try:
            stack.append(Decimal(match.group(0)))
        except ValueError:
            pass

    env.POPUP("COMMAND OUTPUT", data)

################################################################################
# DISPLAY/FORMATTING/SETTINGS/HELP COMMANDS

def _help(stack, arg):
    """display help"""

    lines_entry = [
        "==ENTRY FORMATS==",
        "tagged values      value\"tag",
        "duration           day:hour:min:sec",
        "complex            real j cplx",
        "fraction           num,den",
        "                   whole,num,den",
        "scientific         1e5",
        "engineering        8k2",
        "negatives          _3.2",
        "radix              0b10 0o10 0d10 0x10",
        ""
    ]

    lines_left = []
    lines_right = []

    for key in COMMANDS.keys():
        val = COMMANDS[key]
        if isinstance(key, int):
            lines_left.append(" " * 26)
            lines_left.append("%-26s" % val)
        else:
            doc = val.__doc__
            if doc is None:
                doc = "NODOC: " + key
            lines_left.append("%-10s %-15s" % (key, doc.partition("\n")[0]))

    for key in SINGLE_KEY_COMMANDS.keys():
        key_name = curses_stuff.KEY_TO_NAME.get(key, key)
        val = SINGLE_KEY_COMMANDS[key]
        if val is None or key_name is None:
            continue
        lines_right.append("%-10s %-15s" % (key_name, val.__doc__.partition("\n")[0]))

    if len(lines_left) < len(lines_right):
        lines_left.extend([""] * (len(lines_right) - len(lines_left)))
    if len(lines_left) > len(lines_right):
        lines_right.extend([""] * (len(lines_left) - len(lines_right)))

    lines = lines_entry
    lines += [i + "    " + j for i, j in zip(lines_left, lines_right)]

    env.POPUP("HELP", lines)

def _exc(stack, arg):
    """display the latest exception"""
    if env.LAST_ERROR is None:
        return
    last_bt = traceback.format_exception(*env.LAST_ERROR)
    env.POPUP("LAST BACKTRACE", last_bt)

def _repr(stack, arg):
    """debug: repr(x)"""
    stack.append(repr(stack.pop()))

def _eval(stack, arg):
    """debug: eval(x)"""
    stack.append(eval(stack.pop()))

def _str(stack, arg):
    """debug: stringify"""
    stack.append(env.FORMAT.format(stack.pop()))

def _nat(stack, arg):
    """natural mode"""
    env.FORMAT = nums.NaturalMode()

def _all(stack, arg):
    """all digits mode"""
    env.FORMAT = nums.AllMode()

def _fix(stack, arg):
    """fixed mode, digits = 6 or command argument"""

    if arg is not None and arg.strip():
        digits = int(arg.strip())
    else:
        digits = 6

    env.FORMAT = nums.FixMode(digits=digits)

def _prec(stack, arg):
    """set or query precision in digits"""
    if arg is not None and arg.strip():
        decimal.getcontext().prec = int(arg.strip())

        for i, v in enumerate(stack):
            if isinstance(v, (Decimal, Comflex)):
                stack[i] = v.normalize()
    else:
        stack.append(Decimal(decimal.getcontext().prec))

def _eng(stack, arg):
    """engineering mode, sig figs = 7 or command argument"""

    if arg is not None and arg.strip():
        sigfigs = int(arg.strip())
    else:
        sigfigs = 7

    env.FORMAT = nums.EngMode(sigfigs)

def _sci(stack, arg):
    """scientific mode"""

    if arg is not None and arg.strip():
        digits = int(arg.strip())
    else:
        digits = 6

    env.FORMAT = nums.SciMode(digits)

def _hex(stack, arg):
    """hexadecimal display mode"""
    env.FORMAT = nums.HexMode()

def _dec(stack, arg):
    """decimal display mode"""
    env.FORMAT = nums.DecMode()

def _oct(stack, arg):
    """octal display mode"""
    env.FORMAT = nums.OctMode()

def _bin(stack, arg):
    """binary display mode"""
    env.FORMAT = nums.BinMode()

def _dur(stack, arg):
    """duration display mode"""
    env.FORMAT = nums.TimeDurationMode()

def _auto(stack, arg):
    """automatic display mode"""
    env.FORMAT = nums.AutoMode()

def _wfrac(stack, arg):
    """whole+fraction display mode"""
    if not isinstance(env.FORMAT, nums.WfracMode):
        env.FORMAT = nums.WfracMode(env.FORMAT)

################################################################################
# ADDING SINGLE-LETTER COMMANDS
#
# Insert a tuple into the OrderedDict below:
# (key, command)
#
# key:      the key returned by curses. For printables, just the character.
# command:  a function that takes the current UndoStack, operates on it,
#               and returns nothing. Any exceptions thrown in here will be
#               caught and handled, so don't worry about those.
#
# The command must have a docstring. The first line of this will be used as its
# help text.
#
# Standard unary and binary operators can be made with UNARY() and BINARY(),
# which accept a lambda function taking one or two arguments and return an
# operator closure around it. These take the desired docstring as the second
# argument.

SINGLE_KEY_COMMANDS = collections.OrderedDict([
    ("\x7f", _drop),
    ("\x08", _drop),
    (curses.KEY_BACKSPACE, _drop),

    ("\r", _dup),
    ("\n", _dup),
    (curses.KEY_ENTER, _dup),

    (curses.KEY_UP, _rotup),
    (curses.KEY_DOWN, _rotdown),

    ("-", BINARY(lambda x, y: x - y, "subtract")),
    ("+", BINARY(lambda x, y: x + y, "add")),
    ("*", BINARY(lambda x, y: x * y, "multiply")),
    ("/", BINARY(lambda x, y: x / y, "divide")),
    ("%", BINARY(lambda x, y: dec_divmod(x, y)[1], "modulo")),
    ("^", BINARY(lambda x, y: x ** y, "power")),
    ("E", UNARY(lambda x: x.exp(), "exponential")),
    ("l", _log),
    ("L", UNARY(autoc("log10"), "log base 10")),
    ("r", UNARY(lambda x: x ** Decimal("0.5"), "sq root")),
    ("i", UNARY(lambda x: 1/x, "reciprocal")),
    ("n", UNARY(lambda x: -x, "negate")),
    ("!", UNARY(lambda x: decgamma(x + 1), "factorial")),
    ("P", BINARY(lambda x, y: decgamma(x+1) / decgamma(x-y+1), "permutations")),
    ("C", BINARY(lambda x, y: decgamma(x+1) / (decgamma(y+1)*decgamma(x-y+1)), "combinations")),

    ("<", BINARY(lambda x, y: x * DecimalScalar(2) ** y, "shift left")),
    (">", BINARY(lambda x, y: x // DecimalScalar(2) ** y, "shift right")),
    ("&", BINARY(lambda x, y: DecimalScalar(int(x) & int(y)), "bitwise AND")),
    ("|", BINARY(lambda x, y: DecimalScalar(int(x) | int(y)), "bitwise OR")),
    ("X", BINARY(lambda x, y: DecimalScalar(int(x) ^ int(y)), "bitwise XOR")),
    ("~", UNARY(lambda x: DecimalScalar(~int(x)), "invert bits")),

    ("u", _undo),
    ("R", _redo),
    ("x", _xchg),
    ("[", _get),
    ("{", _pull),
    ("}", _push),

    ("t", _tag),
    ("T", _untag),

    ("?", _help),

    # RESERVED: these appear inside numbers
    ("e", None),
    ("j", None),
    ("h", None),
    ("d", None),
    ("o", None),
    ("b", None),
    ('"', None),
    (".", None),
])

################################################################################
# ADDING NAMED COMMANDS
#
# Insert a tuple into the OrderedDict below:
# (key, command)
#
# name:     the full command name, including beginning single-quote
# command:  a function that takes the current UndoStack, operates on it,
#               and returns nothing. Any exceptions thrown in here will be
#               caught and handled, so don't worry about those.
#
# The command must have a docstring. The first line of this will be used as its
# help text.
#
# Standard unary and binary operators can be made with UNARY() and BINARY(),
# which accept a lambda function taking one or two arguments and return an
# operator closure around it. These take the desired docstring as the second
# argument.
#
# You can insert section headers into the help text by adding a tuple:
# (id, header)
#
# id:       any unique integer
# header:   header text

COMMANDS = collections.OrderedDict([
    (10, "==TRIGONOMETRY, RAD=="),
    ("'sin", UNARY(autoc("sin"), "sin(rad)")),
    ("'cos", UNARY(autoc("cos"), "cos(rad)")),
    ("'tan", UNARY(autoc("tan"), "tan(rad)")),
    ("'asin", UNARY(autoc("asin"), "asin->rad")),
    ("'acos", UNARY(autoc("acos"), "acos->rad")),
    ("'atan", UNARY(autoc("atan"), "atan->rad")),
    (20, "==TRIGONOMETRY, DEG=="),
    ("'sind", UNARY(degree_trig(autoc("sin")), "sin(deg)")),
    ("'cosd", UNARY(degree_trig(autoc("cos")), "cos(deg)")),
    ("'tand", UNARY(degree_trig(autoc("tan")), "tan(deg)")),
    ("'asind", UNARY(degree_invtrig(autoc("asin")), "asin->deg")),
    ("'acosd", UNARY(degree_invtrig(autoc("acos")), "acos->deg")),
    ("'atand", UNARY(degree_invtrig(autoc("atan")), "atan->deg")),
    ("'deg", UNARY(lambda x: x * 180 / decpi(), "rad->deg")),
    ("'rad", UNARY(lambda x: x * decpi() / 180, "deg->rad")),
    (30, "==HYPERBOLICS=="),
    ("'sinh", UNARY(autoc("sinh"), "sinh")),
    ("'cosh", UNARY(autoc("cosh"), "cosh")),
    ("'tanh", UNARY(autoc("tanh"), "tanh")),
    ("'asinh", UNARY(autoc("asinh"), "asinh")),
    ("'acosh", UNARY(autoc("acosh"), "acosh")),
    ("'atanh", UNARY(autoc("atanh"), "atanh")),
    (40, "==SIMPLE=="),
    ("'exp", UNARY(lambda x: x.exp(), "exponential")),
    ("'log", _log),
    ("'log10", UNARY(autoc("log10"), "log base 10")),
    ("'log2", _log2),
    ("'logn", _logn),
    ("'divmod", _divmod),
    ("'f", _f),
    ("'n", _n),
    ("'err", BINARY(lambda x, y: (x - y) / y, "error X=act Y=nom")),
    ("'perr", BINARY(lambda x, y: 100 * (x - y) / y, "pct error X=act Y=nom")),
    ("'unsigned", _unsigned),
    ("'signed", _signed),
    ("'floor", UNARY(lambda x: DecimalScalar(x.to_decimal_scalar().v.to_integral_value(decimal.ROUND_FLOOR)), "floor")),
    ("'ceil", UNARY(lambda x: DecimalScalar(x.to_decimal_scalar().v.to_integral_value(decimal.ROUND_CEILING)), "floor")),
    (50, "==COMPLEX=="),
    ("'re", UNARY(lambda x: x.real, "real part")),
    ("'im", UNARY(lambda x: x.imag, "imaginary part")),
    ("'conj", _conj),
    ("'reim", _reim),
    ("'abs", _abs),
    ("'angle", _angle),
    ("'polar", _polar),
    ("'fcart", _fcart),
    ("'fpolar", _fpolar),
    (70, "==CONSTANTS=="),
    ("'pi", CONST(decpi, "const PI")),
    ("'2pi", CONST(lambda: 2 * decpi(), "const 2 PI")),
    ("'e", CONST(lambda: DecimalScalar("1").exp(), "const E")),
    ("'j", CONST(Comflex(0,1), "imaginary unit")),
    ("'c", CONST("2.99792458e8", "speed of light, m/s")),
    ("'h", CONST("6.6260755e-34", "Planck constant, J s")),
    ("'k", CONST("1.380658e-23", "Boltzmann constant, J/K")),
    ("'elec", CONST("1.60217733e-19", "Charge of electron, C")),
    ("'e0", CONST("8.854187817e-12", "Permittivity of vacuum, F/m")),
    ("'amu", CONST("1.6605402e-27", "Atomic mass unit, kg")),
    ("'Na", CONST("6.0221367e23", "Avogadro's number, mol^-1")),
    ("'atm", CONST("101325", "Standard atmosphere, Pa")),
    ("'8", CONST(2**8 - 1, "Unsigned 8-bit max")),
    ("'16", CONST(2**16 - 1, "Unsigned 16-bit max")),
    ("'24", CONST(2**24 - 1, "Unsigned 24-bit max")),
    ("'32", CONST(2**32 - 1, "Unsigned 32-bit max")),
    ("'64", CONST(2**64 - 1, "Unsigned 64-bit max")),
    ("'kb", CONST(2**10, "1 kiB in bytes")),
    ("'mb", CONST(2**20, "1 MiB in bytes")),
    ("'gb", CONST(2**30, "1 GiB in bytes")),
    ("'tb", CONST(2**40, "1 TiB in bytes")),
    (80, "==LERPN=="),
    ("'sh", _sh),
    ("'help", _help),
    ("'exc", _exc),
    ("'repr", _repr),
    ("'eval", _eval),
    ("'str", _str),
    ("'prec", _prec),
    ("'wfrac", _wfrac),
    ("'auto", _auto),
    ("'eng", _eng),
    ("'sci", _sci),
    ("'fix", _fix),
    ("'nat", _nat),
    ("'all", _all),
    ("'hex", _hex),
    ("'dec", _dec),
    ("'oct", _oct),
    ("'bin", _bin),
    ("'dur", _dur),
])
