# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

"""Flexible complex type with any Scalar subtype (even mismatched)"""

from decimal import Decimal
from fractions import Fraction
import math

from .scalar import *

class Comflex():
    def __init__(self, real, imag=None):
        if isinstance(real, Fraction):
            self._real = FractionScalar(real)
        elif isinstance(real, Scalar):
            self._real = real
        elif isinstance(real, (Decimal, float, int)):
            self._real = DecimalScalar(real)
        elif isinstance(real, Comflex):
            self._real = real._real
            self._imag = real._imag
            return
        else:
            raise ValueError("Cannot create Comflex from %r" % real)

        if imag is None:
            self._imag = type(self._real)(0)
        elif isinstance(imag, Fraction):
            self._imag = FractionScalar(imag)
        elif isinstance(imag, Scalar):
            self._imag = imag
        elif isinstance(imag, (Decimal, float, int)):
            self._imag = DecimalScalar(imag)
        else:
            raise ValueError("Cannot create Comflex from %r" % imag)

    def __repr__(self):
        return f"Comflex({self.real!r}, {self.imag!r})"

    @classmethod
    def from_polar(cls, absval, angle):
        absval = DecimalScalar(absval)

        real = absval * DecimalScalar(math.cos(angle))
        imag = absval * DecimalScalar(math.sin(angle))

        return cls(real, imag)

    @property
    def real(self):
        return self._real

    @property
    def imag(self):
        return self._imag

    @property
    def v(self):
        return self

    def __add__(self, other):
        other = Comflex(other)
        return Comflex(self.real + other.real, self.imag + other.imag)

    __radd__ = __add__

    def __sub__(self, other):
        return self + -other

    def __rsub__(self, other):
        other = Comflex(other)
        return other.__sub__(self)

    def __neg__(self):
        return -1 * self

    def __mul__(self, other):
        # (a+jb)(c+jd) = ac + jad + jbc - bd = (ac-bd) + (ad+bc)j
        other = Comflex(other)
        a, b = self.real, self.imag
        c, d = other.real, other.imag

        return Comflex(a*c - b*d, a*d + b*c)

    __rmul__ = __mul__

    def __truediv__(self, other):
        # (a+jb)/(c+jd) = ((ac+bd)+(bc-ad)j)/(c^2+d^2)
        other = Comflex(other)
        a, b = self.real, self.imag
        c, d = other.real, other.imag

        num_re = a*c + b*d
        num_im = b*c - a*d
        den = c*c + d*d

        return Comflex(num_re / den, num_im / den)

    def __rtruediv__(self, other):
        other = Comflex(other)
        return other.__truediv__(self)

    def __floordiv__(self, other):
        d = self.__truediv__(other)
        return Comflex(int(d.real), int(d.imag))

    def __rfloordiv__(self, other):
        other = Comflex(other)
        return other.__floordiv__(self)

    def __mod__(self, other):
        raise TypeError("can't mod complex numbers.")

    def __divmod__(self, other):
        raise TypeError("can't mod complex numbers.")

    def __pow__(self, other):
        a, b = self.real, self.imag
        c, d = other.real, other.imag
        arg = self.arg()

        coeff = (a*a + b*b) ** (c/2) * Decimal(math.exp(-d * arg))

        return Comflex(
            coeff * Decimal(math.cos(c * arg + d * Decimal(math.log(a*a + b*b)) / 2)),
            coeff * Decimal(math.sin(c * arg + d * Decimal(math.log(a*a + b*b)) / 2)))

    def exp(self):
        return Comflex(
            math.exp(self.real) * math.cos(self.imag),
            math.exp(self.real) * math.sin(self.imag))

    def log(self):
        rp = math.log((self.real * self.real + self.imag * self.imag) ** Decimal("0.5"))
        ip = math.atan2(self.imag, self.real)
        return Comflex(rp, ip)

    def log10(self):
        return self.log() / Comflex(10).log()

    def sin(self):
        return Comflex(
            math.sin(self.real) * math.cosh(self.imag),
            math.cos(self.real) * math.sinh(self.imag))

    def cos(self):
        return Comflex(
            math.cos(self.real) * math.cosh(self.imag),
            -math.sin(self.real) * math.sinh(self.imag))

    def tan(self):
        return self.sin() / self.cos()

    def sinh(self):
        return (self.exp() - (-self).exp()) / 2

    def cosh(self):
        return (self.exp() + (-self).exp()) / 2

    def tanh(self):
        return self.sinh() / self.cosh()

    def atan(self):
        j = Comflex(0, 1)
        return 1/(2*j) * ((j - self)/(j + self)).log()

    def asin(self):
        j = Comflex(0, 1)
        return 1/j * (j*self + (1 - self*self).abs() ** Decimal("0.5")
                               * (j/2 * (1 - self*self).arg()).exp()).log()

    def acos(self):
        j = Comflex(0, 1)
        return 1/j * (self + j*((1 - self*self).abs() ** Decimal("0.5"))
                            * (j/2 * (1 - self*self).arg()).exp()).log()

    def atanh(self):
        return ((1 + self) / (1 - self)).log() / 2

    def asinh(self):
        j = Comflex(0, 1)
        return (self + ((1 + self*self).abs() ** Decimal("0.5"))
                            * (j/2 * (1 + self*self).arg()).exp()).log()

    def acosh(self):
        j = Comflex(0, 1)
        h = Decimal("0.5")
        return (self + (self + 1)**h * (self - 1)**h).log()

    def arg(self):
        return Decimal(math.atan2(self.imag, self.real))

    def abs(self):
        return (self.real * self.real + self.imag * self.imag) ** Decimal("0.5")

    def __abs__(self):
        return self.abs()

    def normalize(self):
        return Comflex(self.real.normalize(), self.imag.normalize())

    def to_fraction_scalar(self):
        return Comflex(
            self.real.to_fraction_scalar(),
            self.imag.to_fraction_scalar(),
        )

    def to_decimal_scalar(self):
        return Comflex(
            self.real.to_decimal_scalar(),
            self.imag.to_decimal_scalar(),
        )
